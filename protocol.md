# Computer protocol for playing neutrino

This document describes a protocol for communicating with computer
engines that play neutrino. It is inspired by "Universal Chess
Interface" (UCI) by Rudolf Huber and Stefan Meyer-Kahlen.

The full name of the protocol described here is "Universal Neutrino
Interface version `0.1.2`" (UNI 0.1.2).

This protocol uses and refers to the notation schemes "Neutrino
Algebraic Notation version `1.1.1`" and "Neutrino State Notation
version `0.1.1`".

## Overview

The protocol is text based and consists of commands send back and
forth, where each command is terminated by a newline.

The protocol is divided into three stages: setup, idle, and play,
where capabilities are declared and parameters are set in the setup
stage, while the actual neutrino decision making is performed in the
play stage. In the idle stage, the engine should just wait for a new
game to begin.

The protocol consists of two parties: the controller and the
engine. The controller initiates games, sets parameters, decides when
engines should compute etc., while the engine's job is to decide on
the next move based on some game state provided by the controller.

All text must be encoded as UTF-8, though many parts will consist of
the US-ASCII subset of UTF-8. The text must not contain the null
character (`U+0000`). Newlines are either line feed (`U+000A`) or
carriage return (`U+000D`) followed by line feed depending on the
platform. Whitespace within a line can be any non-empty sequence of
spaces (`U+0020`) and horizontal tabs (`U+0009`), however this can be
restricted by the engine in the setup stage. Therefore, the initial
messages from the controller to the engine must use exactly a single
space between tokens within a line.

All lines must be terminated by a newline, and all lines must not
contain a newline within them.

Lines should not be arbitrarily long. Conforming parties only need to
handle lines that are at most 16 KiB long. It is not expected that any
lines would ever be that long in practice. The exception might be in
the `moves_so_far` mentioned below. If this turns out to be a real
problem, this specification would need to be updated.

The protocol is intended to be used over `stdin` and `stdout` pipes,
with the controller spawning the engine as a subprocess
(somehow). This allows the engine to write to `stderr` in a different
format that does not affect the protocol, e.g. stack traces from
random exceptions that may occur, debug output etc. The controller can
of course store these messages in a log somewhere but that is out of
scope for this protocol.

The protocol leans toward the controller being flexible and
accommodating towards engines that only implement basic features.

## Setup stage

The controller initiates the protocol by writing exactly 5 lines:

1. `uni` followed by a space followed by the version number. The
   version number must consist of exactly 3 integers separated by full
   stops. Example: `uni 0.1.2`.
2. `charset` followed by a space followed by the charset. Since only
   UTF-8 is allowed in this version, the line will always be `charset
   UTF-8`.
3. `maximum_memory` followed by a space followed by a positive integer
   indicating the maximum amount of memory the engine may use in
   bytes. For example, for 2 GiB: `maximum_memory 2147483648`.
4. `num_cores` followed by a space followed by a positivie integer
   indicating the maximum number of cores that is allocated to the
   engine. The engine does not have to use all cores allocated to
   it. Example: `num_cores 2`.
5. `uni_hello_end`, which marks the end of this hello message from the
   controller.

From here on out, as mentioned in "Overview", whitespace does not need
to be exactly a single space. The notation `\s` is used to indicate a
non-empty sequence of spaces and/or horizontal tabs.

The engine then responds with some information about itself and its
capabilities. The possible lines and their grammar are listed below.

* `"id" \s "name" \s <name>` states the name of the engine. This line
  is required and must occur exactly once. Example: `id name
  Neutrinomancer 2000 v0.99`.
* `"id" \s "author" \s <author>` states an author of the engine. This
  line is optional and may occur multiple times to indicate multiple
  authors. Example: `id author John Doe III (john@doe.com)`.
* `"capability" \s <capability-name> (\s <capability-value>)*` states
  what the engine is and is not capable of, e.g. which parts of the
  protocol it implements or which game modes it supports. See the
  "Capabilities" section for a list these capabilities and their
  default values. These lines are optional and may occur multiple
  times. Example: `capability whitespace single_space`.

Note that `<name>`, `<author>`, `<capability-name>`, and
`<capability-value>` must not contain any newlines.

After these lines the engine must send `uni_ok` to indicate that it is
done stating capabilities. It is the intention that the engine
responds with these lines immediately (say within 1 second). The
engine must not begin computing neutrino plays by e.g. filling a
tablebase; that would be cheating.

The protocol then enters the idle stage where the engine waits for the
controllor to start a new game, or for the controller to stop the
engine.

### Example

The controller sends:

```
uni 0.1.2
charset UTF-8
maximum_memory 1073741824
num_cores 4
uni_hello_end
```

The engine then sends:

```
id name Neutrinomancer 2000 v0.99
capability nan_version 1.1.1
capability nsn_version 0.1.1
capability whitespace single_space
capability partial_turns true
capability game_state nsn
capability tc_increment true
capability tc_hourglass true
capability stop_go true
uni_ok
```

## Idle stage

To allow the engine to shut down gracefully, the controller can send
`quit` to the engine. The engine should then shut itself down as soon
as possible. The engine might be forcefully shut down if it is taking
too long.

To start a new game the controller first sends the line `"new_game" \s
<game-mode>`, where the only possible value for `<game-mode>` is
`neutrino_standard` in this version of the specification.

The controller then sends lines describing the rules and parameters of
the game. These depend on the game mode, but that is only relavant if
other game modes are introduced in some later version of this
spec. The parameters must satisfy the capabilities The following lines
are required.

* `"player" \s ("W" | "B")` states whether the engine is the first
  (`W`) or the second (`B`) player.

The following lines are optional.

* `"tc_initial_time" \s <num-ms>` states the initial time control
  time in milliseconds.
* `"tc_increment" \s <num-ms>` states the time control increment per
  turn in milliseconds. This amount is added to the players clock
  after each turn.
* `"tc_delay" \s <num-ms>` states the time control delay per turn in
  milliseconds. The players clock does not begin to count down until
  `<delay>` milliseconds time has passed. One can think of it as an
  increment that cannot be stockpiled. This is sometimes known as
  "simple delay" or "US delay" as opposed to "Bronstein delay".
* `"tc_overtime" \s <num-moves> \s <num-ms>` states that `<num-ms>`
  milliseconds is added to a players clock after `<num-moves>` moves,
  where a move consist of one turn per player.
* `"tc_hourglass" \s <num-ms> \s <num-ms>` states that an hourglass
  time control is to be used, with the first `<num-ms>` indicating
  initial time of the first player (`W`) in milliseconds, and the
  second `<num-ms>` the initial time for the second player (`B`). In
  an hourglass time control, as your clock is decremented your
  opponents clock is incremented by an equal amount.
* `"tc_max_per_turn" \s <num-ms>` states how many milliseconds a turn
  must maximally take.
* `"max_depth" \s <num-turns>` states the maximum depth the engine is
  allowed to search in its tree of turns (if it has such a tree). The
  depth is measured in turns (remember, 1 turn = move neutrino + move
  own piece).
* `"max_nodes" \s <num-nodes>` states the maximum number of nodes the
  engine can visit in its tree of turns (if it has such a tree).
* `"move_limit \s <num-moves>"` states that game will conclude in a draw
  if it passes that many moves, where once again a move consist of one
  turn per player.
* `"custom_parameter" \s <custom-parameter-name> (\s
  <custom-parameter-value>)*` is for providing engine specific
  parameters. For example, if the engine implements a contempt
  parameter, that could perhaps be set by `custom_parameter contempt
  0.5`. Multiple `custom_parameter` lines can be included to provide
  multiple engine specific parameters.

All the `<num-*>` terms are integers.

Note that the engine does not necessarily need to keep track of all
the time control values: It is provided its own and its opponent's
remaining time before each turn in the play stage.

The controller must send `new_game_parameters_end` to state it is
done describing game parameters.

The engine may allocate datastructures, e.g. an array with `max_nodes`
entries, but it must not begin calculating neutrino moves. The
distinction between allocation and precalculation is a bit vague;
please don't abuse it, though.

When the engine has understood the game parameters and is ready, it
responds with `ready_for_new_game` and the protocol enters the play
stage.

### Example

The controller sends:

```
new_game neutrino_standard
player B
tc_initial_time 300000
tc_increment 5000
max_depth 50
move_limit 10000
new_game_parameters_end
```

The engine then sends:

```
ready_for_new_game
```


## Play stage

In the play stage, the controller first sends the current game state
to the engine, then it tells the engine to start calculating, and
after some time the engine sends its result back to the
controller. This then repeats until the game has reached a conclusion,
or it is stopped by the controller.

### Game state

Depending on the engine's capabilities the game state will either be
represented as (1) a sequence of moves since the start board, (2) the
opponent's previous turn, or (3) the game state in NSN.

To describe the grammar of these options we use terms (such as
`<move>` and `<state>`) from the NSN and the NAN
specifications. However, because this protocol is mainly read by
computers and because we wish to fit into our line based protocol, we
will change some of the delimiters used to `\s`.

```
<game-state> := "moves_so_far" \s "none"
              | "moves_so_far" (\s <move>)+
              | "last_turn" \s "none"
              | "last_turn" \s <turn>
              | "nsn" \s <state>
```

The `"none"` cases correspond to the start of the game.

The line the controller sends is `"game_state" \s <game-state>`.

The engine can then respond with either

* `ready_for_go`, if it has understood the state message and is ready
  to calculate, or
* `send_full_state`, if it does not understand the state message,
  e.g. if its internal board is out of sync with the controllers
  board. The controller then responds with `"game_state" \s "nsn" \s
  <state>` and the controller waits for a `ready_for_go` again.

The engine must not begin its calculation yet. It must wait for a "go"
from the controller.

### Begin calculation

To actually start the calculation, the controller sends a "go" line to
the engine. This line can contain some information about the players'
clocks and other time control data. The grammar for the line is

```
<go-line> := "go" \s "w_time" \s <num-ms> \s "b_time" <num-ms> (\s moves_to_overtime \s <num-moves> \s <num-ms>)
           | "go" \s "infinite"
```

As soon as this line is send the controller begins counting down the
clock.

The `go infinite` case means that the engine is not under any time
control. If it is under no other constraints it is allowed to continue
searching until it is told to stop. It may also stop on its own,
e.g. if it has done searching down to `max_depth` or it it just feels
like it.

While the engine is calculating (with or without time control), the
controller may send `stop_go` to the engine (if it is supported in its
capabilities). The engine should then stop calculating as soon as
possible. It must still return a turn to play as if it has stopped on
its own (more on that later).

While the engine is calculating, it may send statistics and other
information back to the controller. These message types are summarised
in the following grammar.

```
<calc-info> := "depth" \s <num-turns>
             | "nodes" \s <num-nodes>
             | "nodes_per_second" \s <num-nodes>
             | "time" \s <num-ms>
             | "score" \s "mate" \s <num-moves>
             | "score" \s "points" \s <float>
             | "score" \s "probabilities" \s <float> \s <float> \s <float>
<best-line> := "best_line" (\s <move>)+
```

* `depth` is the reached depth in the engines tree of turns (if it has
  such a tree).
* `nodes` is the number of visited nodes in the engines tree of turns.
* `nodes_per_second` is the number of nodes visited per second.
* `time` is the time in milliseconds since the engine received its
  "go".
* `score` is a way of stating which player is in the lead and by how
  much.
  - `mate` is the maximum number of moves until one player is
    guaranteed to win. If the second player (`B`) is winning, the
    number is negated (similar to chess).
  - `points` is some scoring function. At this point it is not clear
    what a good scoring system for neutrino is, but 0 should mean that
    the game is even, a positive number should mean that `W` is
    winning, and a negative number should mean that `B` is winning.
  - `probabilities` are estimated probabilities of `W` winning, a
    draw, and `B` winning, respectively. The numbers must be between 0
    and 1.
* `best_line` is the best sequence of moves the engine has found. Here
  best means that both players play as optimally as the engine can
  calculate.

It is up to the engine to decide which statistics it wishes to provide
and how often, but please do not send thousands of lines per second.

The statistics is gathered in a single line in the following way.

```
<status-line> := "info" (\s <calc-info>)* (\s <best-line>)?
```

It is recommended to send a status line just before sending back the
best turn found.

### Send best turn found

Once the engine has finished calculating, it sends the best turn found
back to the controller as `"best_turn" \s <turn>`. In the case that
the controller specified how the neutrino was moved, the engine must
use an underscore (`_`) or the specifed neutrino move as the first
token in `<turn>`.


At this point, the controller stops subtracting time from the engine's
time control clock and checks that the provided turn is valid.

In the meantime, the engine waits for a new game state again. While it
waits it must not continue calculating moves, but it is allowed to do
cleanup, garbage collection etc.

### Stopping the game

To stop the current game and return to the idle stage, the controller
can send `stop_game` to the engine. The engine must then respond with
`game_stopped` once it has returned to its idle stage and is ready to
start a new game.

### Examples

In these examples, the engine is slow and bad at playing neutrino, so
do not pay too much attention to `nodes_per_second` or the chosen
turns.

#### `moves_so_far`

In this example, the engine is `W`.

The controller sends:

```
game_state moves_so_far none
```

The engine sends:

```
ready_for_go
```

The controller sends:

```
go w_time 100000 b_time 100000
```

The engine sends (one line at a time):

```
info depth 4 nodes 34
info depth 5 nodes 60 score probabilities 0.1 0.8 0.1
info time 3405 best_line 1. b4 ↑d4 d2 ↓c2 2. e2 ↗e3
best_turn b4 ↑d4
```

After a while (the engine's opponent has to play), the controller sends:

```
game_state moves_so_far 1. b4 ↑d4 a4 ↓e2
```

The engine sends:

```
ready_for_go
```

The controller sends:

```
go w_time 97517 b_time 100058
```

The engine sends (one line at a time):

```
info depth 7 nodes 230 nodes_per_second 553 best_line 2. a2 ↙b2 c4 ←c2 3. c3 ↑a4 d2 ↘c3
info depth 7 nodes 338 nodes_per_second 520 best_line 2. a2 ↙b2 c4 ←c2 3. c3 ↑a4 d2 ↓b2
best_turn a2 ↙b2
```

The controller sends:

```
stop_game
```

The engine sends:

```
game_stopped
```

#### `last_turn`

In this example, the engine is `B`.

The controller sends:

```
game_state last_turn c4 ↗e3
```

The engine sends:

```
ready_for_go
```

The controller sends:

```
go infinite
```

The engine sends (one line at a time):

```
info time 1003 depth 1 nodes 1
info time 1304 depth 1 nodes 3
```

Then, while the engine is still calculating, the controller sends:

```
stop_go
```

The engine very quickly sends

```
best_turn e4 ↙a3
```

After a while (the engine's opponent has to play), the controller sends:

```
game_state last_turn c2 ↖c5
```

The engine (being confused about the board state) sends:

```
send_full_state
```

The controller sends:

```
game_state nsn BBWBBC/EEEEEC/BEEEEC/EENEEC/WWEWWC BN 1
```

The engine sends:

```
ready_for_go
```

The controller sends:

```
go infinite
```

The engine is not quick enough to send anything before the controller sends:

```
stop_go
```

The engine very quickly sends

```
best_turn c4 ↘c1
```

The controller sends:

```
stop_game
```

The engine sends:

```
game_stopped
```

#### `nsn`

In this example, the engine is `W` and the neutrino has been moved
before the engine is engaged. The game has also been set up with
`max_nodes 999999`.

The controller sends:

```
game_state nsn BBBBBC/EEEEEC/NEEEEC/EEEEEC/WWWWWC WO 0
```

The engine sends:

```
ready_for_go
```

The controller sends:

```
go infinite
```

The engine sends (one line at a time):

```
info time 1000 nodes 423894 nodes_per_second 423894
info time 2000 nodes 843549 nodes_per_second 421774
```

Then, while the engine is still calculating, the controller sends:

```
stop_go
```

The engine very quickly sends

```
best_turn a3 ↗e2
```

The controller sends:

```
game_state nsn BBEBBC/EEEEEC/EEEEBC/NEEEEC/WWWWWC WN 1
```

The engine sends:

```
ready_for_go
```

The controller sends:

```
go infinite
```

The engine sends (one line at a time):

```
info time 1500 nodes 593763 nodes_per_second 395842 score mate 99
info time 2521 nodes 999999 nodes_per_second 396667 score mate 98
best_turn c4 ←d1
```

The controller sends:

```
stop_game
```

The engine sends:

```
game_stopped
```

## Capabilities

| capability name          | capability values           | description                                                                                       | default                                 |
|--------------------------|-----------------------------|---------------------------------------------------------------------------------------------------|-----------------------------------------|
| `nsn_version`            | `<int> "." <int> "." <int>` | Forces the controller to communicate using that NSN version.                                      | the most recent NSN version             |
| `nan_version`            | `<int> "." <int> "." <int>` | Forces the controller to communicate using that NAN version.                                      | the most recent NAN version             |
| `whitespace`             | `<identifier>`              | Sets how tokens are allowed to be separated in messages by the controller.                        | `any`                                   |
| `game_state`             | `<identifier>`              | Sets how the controller sends game state before a "go" command.                                   | `nsn`                                   |
| `partial_turns`          | `<bool>`                    | States whether the engine can start a turn calculation where the neutrino already has been moved. | only start at the beginning of a turn   |
| `stop_go`                | `<bool>`                    | States whether the engine supports being stopped as it calculates neutrino moves.                 | `stop_go` is not supported              |
| `go_infinite`            | `<bool>`                    | States whether the engine supports `go infinite` commands. If set, `stop_go` should also be set.  | `go infinte` is not supported           |
| `time_control`           | `<bool>`                    | States whether the engine supports time control. If not `go_infinite` should be set.              | time control is supported               |
| `tc_increment`           | `<bool>`                    | States whether the engine supports time control increments.                                       | time control increment is not supported |
| `tc_delay`               | `<bool>`                    | States whether the engine supports time control delays.                                           | time control delay is not supported     |
| `tc_overtime`            | `<bool>`                    | States whether the engine supports time control overtime.                                         | time control overtime is not supported  |
| `tc_hourglass`           | `<bool>`                    | States whether the engine supports time control hourglass.                                        | time control hourglass is not supported |
| `tc_max_per_turn`        | `<bool>`                    | States whether the engine supports a time limit per turn.                                         | max time limit is not supported         |
| `configurable_max_depth` | `<bool>`                    | States whether the engine supports limiting the maximum search depth                              | max depth is not configurable           |
| `configurable_max_nodes` | `<bool>`                    | States whether the engine supports limiting the maximum number of nodes searched                  | max nodes is not configurable           |
| `move_limit`             | `<bool>`                    | States whether the engine can understand games that draw after a certain number of moves          | move limit is not understood            |

For `whitespace` the possible identifiers are the following four.

* `single_space`, in which case a single space is used to separate
  tokens.
* `single_tab`, in which case a single tab is used to separate tokens.
* `single_tab_or_space`, in which case either a single tab or a single
  space is used to separate tokens. The controller may switch
  between tabs and spaces.
* `any`, in which case some non-empty sequence of tabs and spaces are
  used to separate tokens. The controller may use difference
  sequences.

Note that for the `single_*` cases, the engine is still allowed to
delimit tokens with multiple tabs and/or spaces; the controller must
always handle `any`.

For `game_state` the possible values are `moves_so_far`, `last_turn`,
and `nsn`, and they correspond to the similarly named choices for
`<game-state>` mentioned in the "Play stage" secition above.

## Big example

Here is an entire session of back and forth between the controller and
the engine. `<<<...` and `>>>...` lines indicate who is sending in the
following lines. Empty lines are not send and only used for visual
clarity here!

```
>>> Controller sends >>>
uni 0.1.2
charset UTF-8
maximum_memory 1073741824
num_cores 4
uni_hello_end

<<<<< Engine sends <<<<<
id name Neutrinomancer 2000 v0.99
capability nan_version 1.1.0
capability nsn_version 0.1.1
capability whitespace single_space
capability partial_turns true
capability game_state nsn
capability tc_increment true
capability tc_hourglass true
capability stop_go true
uni_ok

>>> Controller sends >>>
new_game neutrino_standard
player W
tc_initial_time 300000
tc_increment 5000
new_game_parameters_end

<<<<< Engine sends <<<<<
ready_for_new_game

>>> Controller sends >>>
game_state nsn BBBBBC/EEEEEC/EENEEC/EEEEEC/WWWWWC WN 0

<<<<< Engine sends <<<<<
ready_for_go

>>> Controller sends >>>
go w_time 300000 b_time 300000

<<<<< Engine sends <<<<<
info depth 4 nodes 34
info depth 5 nodes 60 score probabilities 0.1 0.8 0.1
info time 3405 best_line 1. b4 ↑d4 d2 ↓c2 2. e2 ↗e3
best_turn b4 ↑d4

>>> Controller sends >>>
game_state nsn EBBBBC/EEEWEC/EEBEEC/EEENEC/WWWEWC WN 1

<<<<< Engine sends <<<<<
ready_for_go

>>> Controller sends >>>
go w_time 270030 b_time 199701

<<<<< Engine sends <<<<<
info depth 19 nodes 1300846

>>> Controller sends >>>
stop_go

<<<<< Engine sends <<<<<
info depth 19 nodes 1300847
best_turn e3 ↗d2

>>> Controller sends >>>
stop_game

<<<<< Engine sends <<<<<
game_stopped

>>> Controller sends >>>
quit
```
