# Notation for neutrino

This document describes a notation for games of neutrino as a series
of moves, as well as a notation for neutrino game state (if I ever
get around to defining that).

The full name of the move-series specification is "Neutrino Algebraic
Notation version `1.1.1`" (NAN 1.1.1).

The full name of the game-state specification is "Neutrino State
Notation version `0.1.1`" (NSN 0.1.0). This has a lower version number
as I haven't thought as much about it yet so it will probably change.

## Metanotation

Before we can define the notation for the game, we must first describe
what the notation should map to.

The squares on the a neutrino board is numbered in the following way.

```
  ┌───┬───┬───┬───┬───┬───┐
5 │ B │ B │ B │ B │ B │ C │
  ├───┼───┼───┼───┼───┼───┤
4 │   │   │   │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
3 │   │   │ N │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
2 │   │   │   │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
1 │ W │ W │ W │ W │ W │ C │
  └───┴───┴───┴───┴───┴───┘
    a   b   c   d   e   f
```

Here the 3 beverages chosen are weissbier (`W`, the starting player),
bock (`B`, the other player), and cider (`C`, unmovable bottles in the
side of the crate).

## Notation for moves

The notation is inspired by algebraic notation from chess, in that the
notation is a sequence of piece moves, and each piece moves tells
where a piece ends up on the board. To disambiguate pieces, the
direction of the move is included in the description.

We first describe a position by using the numbering of the neutrino
board mentioned in the "Metanotation" section above.

```
<pos> := <col> <row>
<col> := "a" | "b" | "c" | "d" | "e" | "f"
<row> := "1" | "2" | "3" | "4" | "5"
```

On a player's turn, the player first moves the neutrino followed by
one of the player's own pieces. Since "the neutrino" is an unambiguous
reference, we do not provide a direction for it. A player can also
win, lose, or draw the game after each piece move. A player's turn can
therefore be described by the following grammar.

```
<turn> := <turn-action> (" " <conclusion>)?
        | <conclusion>
<turn-actions> := <pos> " " <dir> <pos>
                | <pos>
                | <pos> " _"
                | "_ " <dir> <pos>
                | "_ _"
                | "_"
<dir> := "↑" | "↗" | "→" | "↘" | "↓" | "↙" | "←" | "↖"
<conclusion> := "W" | "B" | "D"
```

Note that the arrows in `<dir>` are unicode charactes in the range
`U+2190` to `U+2199`. The values in `<conclusion>` corresponds to a
win for the first player (`W`), a win for the second player (`B`), and
a draw (`D`).

An underscore indicates that the action is unspecified. For example,
in the turn `_ ←a1`, the player moved their own piece left to `a1`,
but it is not mentioned how the neutrino was moved. This is also what
makes the difference between the `| <pos>` case and `| <pos> " _"`
case. `| <pos>` means that neutrino has been moved, but the players
own piece has not been moved (yet), while `| <pos> " _"` means that
the players own piece has been moved to some unspecified square.

Underscores can be used to filter out irrelevant information, for
example when only talking about some suffix of a sequence of moves:
"in this position, where the neutrino has already been moved, I would
finish my turn with `_ ←a1`".

For `<turn>`, the `| <conclusion>` case is used to indicate that the
game concluded before a move had been made, but after the current
player had gotten the turn. This can for example be used to
distinguish between the previous player winning with their move and
the current player running out of time.

One move of the game consists of a turn by each player. To help humans
reading the notation, the move number is noted along with the turns.

```
<move> := <count> ". " <turn> " " <turn>
        | <count> ". " <turn>
```

Here `<count>` is a positive integer written in decimal notation.

If a turn only consists of a single `<pos>` or if it ends with a
`<conclusion>`, then it must not be followed by another move.

A game then consists of a secuence of moves seperated by a newline. A
final move should also followed by a newline.

```
<game> := (<move> <move-delim>)*
<move-delim> := "\n"
```

Only the last move is allowed to only have a single turn. Similarly,
if a move ends in a conclusion it should not be followed by any more
moves.

### Partial games

If the game is ongoing (having reached a conclusion yet), the game can
be written as a prefix of the grammar above, e.g. stopping after the
first `<pos>` in a turn, if the player has only moved the neutrino
(and not their own piece yet) in their turn.

Note that games can be concluded in the middle of a player's
turn. This allows the notation to handle resignations or other house
rules, e.g. if the player is under time control and time runs out
after moving the neutrino but before moving a player piece.

### Examples

#### Quick game

In this example the second player wins after a few (weird) moves, by
moving the neutrino the first row.

```
1. b4 ↑d4 d2 ↓c2
2. e2 ↗e3 c1 B
```

The final board state:

```
  ┌───┬───┬───┬───┬───┬───┐
5 │ B │ B │   │ B │ B │ C │
  ├───┼───┼───┼───┼───┼───┤
4 │   │   │   │ W │   │ C │
  ├───┼───┼───┼───┼───┼───┤
3 │   │   │   │   │ W │ C │
  ├───┼───┼───┼───┼───┼───┤
2 │   │   │ B │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
1 │ W │ W │   │ N │ W │ C │
  └───┴───┴───┴───┴───┴───┘
    a   b   c   d   e   f
```

#### Swap

```
1. c4 ↗e3 e4 ↙a3
2. c2 ↖c5 c4 ↘c1
```

```
  ┌───┬───┬───┬───┬───┬───┐
5 │ B │ B │ W │ B │ B │ C │
  ├───┼───┼───┼───┼───┼───┤
4 │   │   │ N │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
3 │   │   │   │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
2 │   │   │   │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
1 │ W │ W │ B │ W │ W │ C │
  └───┴───┴───┴───┴───┴───┘
    a   b   c   d   e   f
```

#### Trap

```
1. c4 ↖a4 a2 ↙b2
2. a3 ↖b4 a2 ↙b3
3. a3 ↑a2 W
```

```
  ┌───┬───┬───┬───┬───┬───┐
5 │ B │ B │ B │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
4 │ W │ W │   │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
3 │ N │ B │   │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
2 │ W │ B │   │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
1 │   │ W │ W │   │   │ C │
  └───┴───┴───┴───┴───┴───┘
    a   b   c   d   e   f
```

## Notation for game state

To record the game state we need to describe the board layout as well
as whose turn it is.

To describe the board, we do this row by row, starting with the
topmost row (row 5). We use `W`, `B`, `N`, and `C` to describe pieces
(player 1, player 2, neutrino, and unmovable side pieces
respectively), and `E` to describe empty squares.

```
<row> := <square> <square> <square> <square> <square> <square>
<square> := "W" | "B" | "N" | "C" | "E"
```

Each row is delimited by a `/`.

```
<board> := <row> "/" <row> "/" <row> "/" <row> "/" <row>
```

After the board, we write down the current player and which piece they
are about to move. Here we ignore that players stop playing after the
game has concluded.

```
<stage> := <player> <piece>
<player> := "W" | "B"
<piece> := "N" | "O"
```

For `<player>`, `W` denotes the first player and `B` denotes the
second player. For `<piece>`, `N` denotes the neutrino and `O` denotes
the player's own piece.

Finally, we record the number of moves played, i.e. how many times the
second player has finished their turn.

The combined notation is the board and game stage separated by a space:

```
<state> := <board> " " <stage> " " <num-moves-played>
```

### Examples

#### Early game

Recall that the starting position is

```
  ┌───┬───┬───┬───┬───┬───┐
5 │ B │ B │ B │ B │ B │ C │
  ├───┼───┼───┼───┼───┼───┤
4 │   │   │   │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
3 │   │   │ N │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
2 │   │   │   │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
1 │ W │ W │ W │ W │ W │ C │
  └───┴───┴───┴───┴───┴───┘
    a   b   c   d   e   f
```

which would correspond to

```
BBBBBC/EEEEEC/EENEEC/EEEEEC/WWWWWC WN 0
```

After moving the neutrino to the left, the board an notation would be

```
  ┌───┬───┬───┬───┬───┬───┐
5 │ B │ B │ B │ B │ B │ C │
  ├───┼───┼───┼───┼───┼───┤
4 │   │   │   │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
3 │ N │   │   │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
2 │   │   │   │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
1 │ W │ W │ W │ W │ W │ C │
  └───┴───┴───┴───┴───┴───┘
    a   b   c   d   e   f
```

and

```
BBBBBC/EEEEEC/NEEEEC/EEEEEC/WWWWWC WO 0
```

Then one of the pieces is moved diagonally resulting in

```
  ┌───┬───┬───┬───┬───┬───┐
5 │ B │ B │ B │ B │ B │ C │
  ├───┼───┼───┼───┼───┼───┤
4 │   │   │   │   │ W │ C │
  ├───┼───┼───┼───┼───┼───┤
3 │ N │   │   │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
2 │   │   │   │   │   │ C │
  ├───┼───┼───┼───┼───┼───┤
1 │ W │   │ W │ W │ W │ C │
  └───┴───┴───┴───┴───┴───┘
    a   b   c   d   e   f
```

and

```
BBBBBC/EEEEWC/NEEEEC/EEEEEC/WEWWWC BN 0
```

#### Nonstandard layout

In this example the unmovable pieces are in unusual positions. The
second player has just moved the neutrino and is about to move a `B`
piece.

```
  ┌───┬───┬───┬───┬───┬───┐
5 │ W │ W │ C │ B │ B │ C │
  ├───┼───┼───┼───┼───┼───┤
4 │   │ N │ B │   │   │   │
  ├───┼───┼───┼───┼───┼───┤
3 │ B │   │   │ B │   │ C │
  ├───┼───┼───┼───┼───┼───┤
2 │   │ W │   │   │   │ W │
  ├───┼───┼───┼───┼───┼───┤
1 │   │   │ C │   │ W │ C │
  └───┴───┴───┴───┴───┴───┘
    a   b   c   d   e   f
```

```
WWCBBC/ENBEEE/BEEBEC/EWEEEW/EECEWC BO 17
```
